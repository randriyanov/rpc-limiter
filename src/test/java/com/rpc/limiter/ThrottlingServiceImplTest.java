package com.rpc.limiter;

import com.rpc.limiter.cache.RpcLimiterCache;
import com.rpc.limiter.cache.UserTokenCache;
import com.rpc.limiter.configuration.ThrottlingConfigurationProperties;
import com.rpc.limiter.model.SLA;
import com.rpc.limiter.service.RpcLimiter;
import com.rpc.limiter.service.SlaService;
import com.rpc.limiter.service.ThrottlingService;
import com.rpc.limiter.service.ThrottlingServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;


public class ThrottlingServiceImplTest {

    private ThrottlingConfigurationProperties configurationProperties = mock(ThrottlingConfigurationProperties.class);
    private SlaService slaService = mock(SlaService.class);
    private RpcLimiter rpcLimiter = new RpcLimiter(10, 1);
    private UserTokenCache tokenCache = mock(UserTokenCache.class);
    private RpcLimiterCache rpcLimiterCache = mock(RpcLimiterCache.class);
    private Set<String> tokensInProcessSet = mock(Set.class);
    private ThrottlingService throttlingService = new ThrottlingServiceImpl(slaService, rpcLimiter, tokenCache, rpcLimiterCache, configurationProperties, tokensInProcessSet);

    private final SLA TEST_SLA = new SLA("Test", 1);
    private final RpcLimiter RPC_LIMITER = new RpcLimiter(10, 1L);


    @BeforeEach
    public void setup() {
        when(configurationProperties.getGuestRpc()).thenReturn(10);
        when(configurationProperties.getRpcPerSecond()).thenReturn(1L);
        when(tokenCache.getSLA("Test")).thenReturn(Optional.of(TEST_SLA));
        when(rpcLimiterCache.getRpcLimiter(TEST_SLA)).thenReturn(Optional.of(RPC_LIMITER));
    }

    private final RpcLimiter testLimiter = new RpcLimiter(10, 1);
    @Test
    public void testRequestsNotAllowedForGuestAfterGetLimit() throws Exception {
        ScheduledExecutorService executor= Executors.newScheduledThreadPool(5);
        Future<Boolean> futureResult = executor.submit(() -> {
            for (int i = 0; i < testLimiter.getAllowedRequests(); i++) {
                Thread.sleep(50);
                throttlingService.isRequestAllowed(Optional.empty());
            }
            return throttlingService.isRequestAllowed(Optional.empty());
        });
        executor.schedule(() -> {
            futureResult.cancel(true);
        }, configurationProperties.getRpcPerSecond(), TimeUnit.SECONDS);
        executor.shutdown();
        executor.awaitTermination(2, TimeUnit.SECONDS);

        assertFalse(futureResult.get());
    }



    @Test
    public void ifTokenPresentInCacheThenProcessIt() throws Exception {
        ScheduledExecutorService executor= Executors.newScheduledThreadPool(5);
        Future<Boolean> futureResult = executor.submit(() -> {
            for (int i = 0; i < configurationProperties.getGuestRpc(); i++) {
                Thread.sleep(90);
                throttlingService.isRequestAllowed(Optional.of("Test"));
            }
            return throttlingService.isRequestAllowed(Optional.of("Test"));
        });
        executor.schedule(() -> {
            futureResult.cancel(true);
        }, configurationProperties.getRpcPerSecond(), TimeUnit.SECONDS);
        executor.shutdown();
        executor.awaitTermination(2, TimeUnit.SECONDS);
        assertFalse(futureResult.get());

        verify(tokenCache, times(configurationProperties.getGuestRpc() + 1)).getSLA("Test");
        verify(rpcLimiterCache, times(configurationProperties.getGuestRpc() + 1)).getRpcLimiter(TEST_SLA);
    }

    @Test
    public void whenTokenInProcessingThenUseGuestRpcLimiter() {
        final String testToken = "TestToken2";
        when(tokensInProcessSet.contains(testToken)).thenReturn(true);
        rpcLimiter = mock(RpcLimiter.class);
        throttlingService = new ThrottlingServiceImpl(slaService, rpcLimiter, tokenCache, rpcLimiterCache, configurationProperties, tokensInProcessSet);
        throttlingService.isRequestAllowed(Optional.of("TestToken2"));

        verify(rpcLimiter, only()).isAllowedRequest();
    }

    @Test
    public void whenSlaServiceIsCalledThenNewSlaWillBeStoredInCache() throws Exception {
        final String testToken = "TestToken";
        when(slaService.getSlaByToken(testToken)).thenReturn(CompletableFuture.completedFuture(TEST_SLA));
        assertFalse(throttlingService.isRequestAllowed(Optional.of("TestToken")));
        Thread.sleep(100L);
        verify(tokensInProcessSet, times(1)).remove("TestToken");
        verify(tokenCache, times(1)).putSLA("TestToken", TEST_SLA);
        verify(rpcLimiterCache, times(1)).putRpcLimiter( any(SLA.class), any(RpcLimiter.class));
    }

}
