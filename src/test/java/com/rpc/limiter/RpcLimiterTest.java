package com.rpc.limiter;

import com.rpc.limiter.service.RpcLimiter;
import org.junit.jupiter.api.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;

public class RpcLimiterTest {

    private final RpcLimiter testLimiter = new RpcLimiter(10, 1);

    @Test
    public void testWhenTimeElapsedThenNoRequestAllowed() throws Exception {

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
        Future<Boolean> futureResult = executor.submit(() -> {
            for (int i = 0; i < testLimiter.getAllowedRequests(); i++) {
                Thread.sleep(90);
                testLimiter.isAllowedRequest();
            }
            return testLimiter.isAllowedRequest();
        });
        executor.schedule(() -> {
            futureResult.cancel(true);
        }, testLimiter.getRpcPerSecond(), TimeUnit.SECONDS);
        executor.shutdown();
        executor.awaitTermination(2, TimeUnit.SECONDS);

        assertFalse(futureResult.get());
    }
}
