package com.rpc.limiter.service;

import com.rpc.limiter.model.SLA;

import java.util.concurrent.CompletableFuture;

public interface SlaService {

    CompletableFuture<SLA> getSlaByToken(String token);
}
