package com.rpc.limiter.service;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.StampedLock;


public class RpcLimiter {

    private StampedLock lock = new StampedLock();
    private final LongAdder guestRpc = new LongAdder();
    private final int allowedRequests;
    private final long rpcPerSecond;
    private volatile long lastTime = System.nanoTime();

    public RpcLimiter(int allowedRequests, long rpcPerSecond) {
        this.allowedRequests = allowedRequests;
        this.rpcPerSecond = rpcPerSecond;
        this.guestRpc.add(allowedRequests);
    }

    private long getElapsedTimeInNano() {
        long readLock = lock.tryOptimisticRead();
        if (!lock.validate(readLock)) {
            try {
                readLock = lock.readLock();
            } finally {
                lock.unlockRead(readLock);
            }
        }
        return System.nanoTime() - lastTime;
    }

    private void resetTime() {
        long writeLock = lock.writeLock();
        try {
            lastTime = System.nanoTime();
        } finally {
            lock.unlockWrite(writeLock);
        }
    }

    public boolean isAllowedRequest() {
        if (isTimePassed()) {
            resetTime();
            guestRpc.reset();
            guestRpc.add(allowedRequests);
        }
        if (guestRpc.longValue() == 0) {
            return false;
        }
        guestRpc.decrement();
        return true;
    }

    private boolean isTimePassed() {
        return TimeUnit.SECONDS.toNanos(rpcPerSecond) < getElapsedTimeInNano();
    }

    public int getAllowedRequests() {
        return allowedRequests;
    }

    public long getRpcPerSecond() {
        return rpcPerSecond;
    }

    public LongAdder getGuestRpc() {
        return guestRpc;
    }
}
