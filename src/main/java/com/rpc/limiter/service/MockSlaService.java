package com.rpc.limiter.service;

import com.rpc.limiter.model.SLA;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class MockSlaService implements SlaService {


    @Override
    public CompletableFuture<SLA> getSlaByToken(String token) {
        return null;
    }

}
