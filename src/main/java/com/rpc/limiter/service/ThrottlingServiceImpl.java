package com.rpc.limiter.service;

import com.rpc.limiter.cache.RpcLimiterCache;
import com.rpc.limiter.cache.UserTokenCache;
import com.rpc.limiter.configuration.ThrottlingConfigurationProperties;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executors;

@Service
public class ThrottlingServiceImpl implements ThrottlingService {

    private final SlaService slaService;
    private final RpcLimiter guestRpcLimiter;
    private final UserTokenCache userTokenCache;
    private final RpcLimiterCache rpcLimiterCache;
    private final ThrottlingConfigurationProperties configurationProperties;
    private final Set<String> tokensInProcess;


    public ThrottlingServiceImpl(final SlaService slaService,
                                 final RpcLimiter guestRpcLimiter,
                                 final UserTokenCache userTokenCache,
                                 final RpcLimiterCache rpcLimiterCache,
                                 final ThrottlingConfigurationProperties configurationProperties,
                                 final Set<String> tokensInProcess) {
        this.slaService = slaService;
        this.guestRpcLimiter = guestRpcLimiter;
        this.userTokenCache = userTokenCache;
        this.rpcLimiterCache = rpcLimiterCache;
        this.configurationProperties = configurationProperties;
        this.tokensInProcess = tokensInProcess;
    }

    @Override
    public boolean isRequestAllowed(final Optional<String> optionToken) {
        if (optionToken.isEmpty()) {
            return guestRpcLimiter.isAllowedRequest();
        }
        final String token = optionToken.get();
        final Optional<RpcLimiter> cachedRpcLimiter = getCachedRpcLimiter(optionToken.get());
        if (cachedRpcLimiter.isPresent()) {
            return cachedRpcLimiter.get().isAllowedRequest();
        }
        if (tokensInProcess.contains(token)) {
            return guestRpcLimiter.isAllowedRequest();
        }
        processTokenAsync(token);
        return false;
    }

    private Optional<RpcLimiter> getCachedRpcLimiter(final String token) {
        return userTokenCache.getSLA(token)
                .flatMap(rpcLimiterCache::getRpcLimiter);
    }

    private void processTokenAsync(final String token) {
        tokensInProcess.add(token);
        slaService.getSlaByToken(token).handleAsync((sla, throwable) -> sla, Executors.newCachedThreadPool())
                .whenComplete((sla, throwable) -> {
            if (throwable == null) {
                tokensInProcess.remove(token);
                userTokenCache.putSLA(token, sla);
                rpcLimiterCache.putRpcLimiter(sla, new RpcLimiter(sla.getRps(),
                        configurationProperties.getRpcPerSecond()));
            }
        });

    }


}
