package com.rpc.limiter.service;

import java.util.Optional;

public interface ThrottlingService {

    boolean isRequestAllowed(Optional<String> token);
}
