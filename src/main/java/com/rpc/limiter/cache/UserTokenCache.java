package com.rpc.limiter.cache;

import com.rpc.limiter.model.SLA;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.Optional;

@Configuration
public class UserTokenCache {

    private final Map<String, SLA> slaCache;

    public UserTokenCache(Map<String, SLA> slaCache) {
        this.slaCache = slaCache;
    }

    public Optional<SLA> getSLA(final String token) {
        return Optional.ofNullable(slaCache.get(token));
    }

    public SLA putSLA(final String token, final SLA sla) {
        return slaCache.putIfAbsent(token, sla);
    }
}
