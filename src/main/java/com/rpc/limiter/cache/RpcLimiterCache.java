package com.rpc.limiter.cache;

import com.rpc.limiter.model.SLA;
import com.rpc.limiter.service.RpcLimiter;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.Optional;

@Configuration
public class RpcLimiterCache {

    private final Map<SLA, RpcLimiter> rpcLimiterCache;

    public RpcLimiterCache(Map<SLA, RpcLimiter> rpcLimiterCache) {
        this.rpcLimiterCache = rpcLimiterCache;
    }

    public Optional<RpcLimiter> getRpcLimiter(final SLA sla) {
        return Optional.ofNullable(rpcLimiterCache.get(sla));
    }

    public RpcLimiter putRpcLimiter(final SLA sla, final RpcLimiter rpcLimiter) {
        return rpcLimiterCache.putIfAbsent(sla, rpcLimiter);
    }
}
