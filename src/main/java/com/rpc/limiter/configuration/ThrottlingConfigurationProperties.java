package com.rpc.limiter.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "throttling")
public class ThrottlingConfigurationProperties {
    private int guestRpc;
    private long rpcPerSecond;

    public int getGuestRpc() {
        return guestRpc;
    }

    public void setGuestRpc(int guestRpc) {
        this.guestRpc = guestRpc;
    }

    public long getRpcPerSecond() {
        return rpcPerSecond;
    }

    public void setRpcPerSecond(long rpcPerSecond) {
        this.rpcPerSecond = rpcPerSecond;
    }
}
