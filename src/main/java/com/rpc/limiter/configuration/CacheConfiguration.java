package com.rpc.limiter.configuration;

import com.rpc.limiter.model.SLA;
import com.rpc.limiter.service.RpcLimiter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class CacheConfiguration {

    @Bean
    public Map<String, SLA> rpcConcurrentMap() {
        return new ConcurrentHashMap<>();
    }

    @Bean
    public Map<SLA, RpcLimiter> slaConcurrentMap() {
        return new ConcurrentHashMap<>();
    }
}
