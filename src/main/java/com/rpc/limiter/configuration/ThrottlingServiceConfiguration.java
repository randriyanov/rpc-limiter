package com.rpc.limiter.configuration;

import com.rpc.limiter.service.RpcLimiter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class ThrottlingServiceConfiguration {

    @Bean
    public RpcLimiter guestRpcLimiter(final ThrottlingConfigurationProperties configurationProperties) {
        return new RpcLimiter(configurationProperties.getGuestRpc(), configurationProperties.getRpcPerSecond());
    }

    @Bean
    public Set<String> tokensInProcess() {
        return Collections.newSetFromMap(new ConcurrentHashMap<>());
    }

}
