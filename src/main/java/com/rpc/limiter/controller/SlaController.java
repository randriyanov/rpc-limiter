package com.rpc.limiter.controller;

import com.rpc.limiter.service.ThrottlingService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController("/")
public class SlaController {

    private final ThrottlingService throttlingService;

    public SlaController(ThrottlingService throttlingService) {
        this.throttlingService = throttlingService;
    }

    @GetMapping("sla")
    public boolean isRequestAllowed(@RequestHeader(name = "Authorization") Optional<String> token) {
        return throttlingService.isRequestAllowed(token);
    }
}
